<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/register.css">
</head>
<body>

	<div class="box" action="index.html" method="post">
		<h1>Sign Up!</h1>
		<div class="parent">
		<div>
			<input id="name" type="text" name="" placeholder="Enter Your name...">
			<input id="surname" type="text" name="" placeholder="Enter Your surname...">
			<input id="age" type="text" name="" placeholder="Enter Your age...">
		</div>
		<div>
		<input id="email" type="text" name="" placeholder="Enter Your email...">
		<input id="password"type="password" name="" placeholder="Enter Your password...">
		<input id="cpassword"type="password" name="" placeholder="Confirm password...">
		</div>
		</div>
		<input id="register" type="submit" name="" value="Sign Up!">
		<button class="btn btn-success" id="register"> <a href="index.php"> Sign In! </a></button></div>
	<h1 class="text-light"></h1>
	</body>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@4/dark.css" rel="stylesheet">
	<script src="//cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js"></script>
	<script src="js/register.js"></script>
	</html>