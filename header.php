<?php 
session_start();
if (isset($_SESSION["user"])) {
	$user = $_SESSION["user"];
}else{
	header("Location:index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/header.css">
  <link rel="stylesheet" href="css/profile.css">
  <link href="lightbox/dist/css/lightbox.min.css" rel="stylesheet" />
</head>
<nav class="navbar navbar-expand-sm bg-dark navbar-dark navbar-right">
  <!-- Brand -->
  <!--  <a class="navbar-brand" href="#">Logo</a> -->

  <!-- Links -->
  <ul class="navbar-nav">
    <li class="nav-item">
      <a class="nav-link active" href="profile.php">My profile</a>
    </li>
   <!--  <li class="nav-item">
      <a class="nav-link" href="#">Link 2</a>
    </li> -->

    <!-- Dropdown -->
  </ul>
  <div class="form-inline">
    <input id="search" class="form-control mr-sm-2" type="text" placeholder="Search">
    <div id="sbox" class="card bg-dark text-light rounded">
      <ul class="list-group sresult">
        <li class="list-group-item border text-dark">
          item1
        </li>
        <li class="list-group-item border">
          item1
        </li>
        <li class="list-group-item border">
          item1
        </li>
      </ul>
    </div>
  </div>
  <ul class="navbar-nav navbar navbar-right">
    <!-- Dropdown -->
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown" aria-expanded="false">

      </a>
      <div class="dropdown-menu">
        <a class="dropdown-item" href="edit_profile.php">Edit Profile</a>
        <a class="dropdown-item" href="password_change.php">Change Password</a>
      </div>
    </li>
  
  </ul>
  <button class="btn btn-white" type="submit"><a href="index.php">Log Out</a></button>
</nav>
<script type="text/javascript" src="js/header.js"></script>
