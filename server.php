<?php 
/**
 * 
 */
class Project {
	private $db;
	function __construct(){
		$this->db=new mysqli("localhost","root","","projectmiqayel");
		$this->db->set_charset("UTF8");
		if ($_SERVER["REQUEST_METHOD"]=="POST") {
			if (isset($_POST["ajax"])) {
				if ($_POST["ajax"]=="register") {
					$this->Register();
				}
				if ($_POST["ajax"]=="login") {
					$this->Login();
				}
				if ($_POST["ajax"]=="search") {
					$this->Search();
				}
				if ($_POST["ajax"]=="edit_profile") {
					$this->Edit_profile();
				}
				if ($_POST["ajax"]=="password_change") {
					$this->Password_change();
				}
				if ($_POST["ajax"]=="getPhotos") {
					$this->getPhoto();
				}
				if ($_POST["ajax"]=="sendRequest") {
					$this->sendRequest();
				}
				if ($_POST["ajax"]=="deleteRequest") {
					$this->deleteRequest();
				}
				if ($_POST["ajax"]=="acceptRequest") {
					$this->acceptRequest();
				}
				if ($_POST["ajax"]=="set_profile_photo") {
					$this->Set_profile_photo();
				}
				if ($_POST["ajax"]=="getUser") {
					$this->getUser();
				}
				//if ($_POST["ajax"]=="change_profile_photo") {
				//	$this->getPhoto();
				//}
			}
			if (isset($_FILES["photo"])) {
				$this->addPhoto();
			}
		}
	}
	function Register(){

		extract($_POST["user"]);
		$mails = $this->db->query("SELECT * from user where email = '$email'")->fetch_all(true);
		// print json_encode($mails);
		// associative zangvaci keyerov popoxakannere stexcum ev valuenery talis e vorpes arjeq
		$errors=[];
		if (empty($name)) {
			$errors["name"]="Enter Your name";
		}
		if (empty($surname)) {
			$errors["surname"]="Enter Your surname";
		}
		if (empty($age)) {
			$errors["age"]="Enter Your age";
		}
		else if(!is_numeric($age)){
			$errors["age"]="Enter number";
		}
		if (empty($email)) {
			$errors["email"]="Enter Your email";
		}
		else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
			$errors["email"]="Enter valid email";
		}
		else if(count($mails) > 0){
			$errors["email"]="Already registered";
		}
		if (empty($password)) {
			$errors["password"]="Enter Your password";
		}
		else if(strlen($password)<6){
			$errors["password"]="Enter minimum 6 symbols";
		}
		else if($password!=$cpassword){
			$errors["password"]="Passwords don`t match";
		}
		if (empty($cpassword)) {
			$errors["cpassword"]="Confirm Your password";
		}
		if (count($errors)>0) {
			print json_encode($errors);
		}
		else{
			print "success";
			$hash=password_hash($password, PASSWORD_DEFAULT);
			$this->db->query("INSERT into user(name, surname, age,email,password,photo) VALUES ('$name', '$surname', '$age','$email','$hash','image/default.jpg')");
		}

	}

	function Login(){
		extract($_POST["user"]);
		$errors = [];
		if (empty($email)) {
			$errors["email"]="Enter Your email";
		}else{
			$user = $this->db->query("SELECT * from user where email = '$email'")->fetch_all(true)[0];
			if(count($user) == 0){
				$errors["email"]="Email is not registered";
			}
			else{
				if (!password_verify($password, $user['password'])) {
					$errors["email"]="Email or password is incorrect";
				}
				else{
					session_start();
					unset($user["password"]);
					$_SESSION["user"]=$user;
					print "success";
				}
			}
		}
		if (count($errors)>0) {
			print json_encode($errors);
		}

	}
	function Search(){
		session_start();
		$id = $_SESSION['user']['id'];
		$search = $_POST["search"];
		$result = $this->db->query("SELECT id,name,surname,age,photo from user WHERE (name LIKE '%$search%' or surname LIKE '%$search%') and id != '$id' ")->fetch_all(true);
		for ($i = 0; $i < count($result); $i++) {
			$result[$i]['info'] = '';
			$d = $result[$i]["id"];
			$s = $this->db->query("SELECT * FROM friends WHERE user_1_id = '$id' AND user_2_id = '$d' ")->fetch_all(true);
			$r_from_me = $this->db->query("SELECT * FROM request WHERE from_id = '$id' AND to_id = '$d' ")->fetch_all(true);
			$r_to_me = $this->db->query("SELECT * FROM request WHERE from_id = '$d' AND to_id = '$id' ")->fetch_all(true);
			if (count($s) > 0) {
				$result[$i]["info"] = "friends";
			}
			else if(count($r_from_me) > 0){
				$result[$i]["info"] = "req_to";
			}
			else if(count($r_to_me) > 0){
				$result[$i]["info"] = "req_me";
			}
			else{
				$result[$i]["info"] = "guest";
			}
		}
		print json_encode($result);
	}
	function Edit_profile(){
		session_start();
		$id=$_SESSION["user"]["id"];
		extract($_POST["user"]);
		$errors = [];
		$mails = $this->db->query("SELECT * from user where email = '$email' and id !='$id'")->fetch_all(true);
		// print json_encode($mails);
		if (empty($name)) {
			$errors["name"]="Enter Your name";
		}
		if (empty($surname)) {
			$errors["surname"]="Enter Your surname";
		}
		if (empty($age)) {
			$errors["age"]="Enter Your age";
		}
		else if(!is_numeric($age)){
			$errors["age"]="Enter number";
		}
		if (empty($email)) {
			$errors["email"]="Enter Your email";
		}
		else if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
			$errors["email"]="Enter valid email";
		}
		// else if($email==$_POST["user"]["email"]){
		// 	$errors["email"]="Already registered";
		// }
		if (count($errors)>0) {
			print json_encode($errors);
		}
		else{
			$this->db->query("UPDATE user SET name = '$name', surname = '$surname', email = '$email', age = '$age' WHERE id='$id' ");
			$_SESSION["user"]["name"]=$name;
			$_SESSION["user"]["surname"]=$surname;
			$_SESSION["user"]["age"]=$age;
			$_SESSION["user"]["email"]=$email;
			print "success";
		}
		
	}
	function Password_change(){

		extract($_POST["user"]);
		session_start();
		$id = $_SESSION["user"]["id"];
		//$mails = $this->db->query("SELECT * from user where email = '$email'")->fetch_all(true);
		// print json_encode($mails);
		// associative zangvaci keyerov popoxakannere stexcum ev valuenery talis e vorpes arjeq
		$errors=[];
		$user = $this->db->query("SELECT * from user where id = '$id'")->fetch_all(true);
		
		if (!password_verify($old_password, $user[0]['password'])) {
			$errors["old_password"]="Enter your current password";
		}
		if(empty($new_password) ){
			$errors["new_password"] = 'Enter new password !';
		}
		else if(strlen($new_password) < 6){
 			$errors["new_password"] = 'Enter minimum 6 symbols !';	
		}
		else if($new_password != $confirm_new_password){
			$errors["confirm_new_password"] = 'Confirm new password !';
		}
		// else if($new_password != $confirm_new_password){
		// 	$errors["confirm_new_password"]="Passwords don`t match";
		// }
		// nor passwordy chpii hni het hamynkni
		if (count($errors)>0) {
			print json_encode($errors);
		}
		else{
			print "success";
			$hash = password_hash($new_password, PASSWORD_DEFAULT);
			$this->db->query("UPDATE user set password = '$hash' where id = '$id'");

		}

	}
	function addPhoto(){
		session_start();
		$id=$_SESSION["user"]["id"];
		// print "Ok";
		$photo=$_FILES["photo"];
		$tmp=$photo["tmp_name"];
		$address="image/".time().$photo["name"];
		if (!file_exists("image")) {
			mkdir("image");
		}
		move_uploaded_file($tmp, $address);
		$this->db->query("INSERT into photo(address,user_id) VALUES ('$address','$id')");
		header("Location:profile.php");
	}
	function getPhoto(){
		//print "ok";
		session_start();
		$id = $_SESSION["user"]["id"];
		$photos=$this->db->query("SELECT * from photo WHERE user_id='$id'")->fetch_all(true);
		print json_encode($photos);
	}



	function sendRequest(){
		session_start();
		$id = $_SESSION['user']['id'];
		// print $_POST['id'];
		$user_id = $_POST['id'];
		$this->db->query("INSERT INTO request(from_id,to_id) VALUES ('$id', '$user_id')");
	}


	function deleteRequest(){
		session_start();
		$id = $_SESSION['user']['id'];
		$user_id = $_POST['id'];
		$this->db->query("DELETE FROM request WHERE from_id = '$id' and to_id = '$user_id'");
	}
	function acceptRequest(){
		session_start();
		$id = $_SESSION['user']['id'];
		$user_id = $_POST['id'];
		$this->db->query(" DELETE FROM request WHERE from_id = '$user_id' and to_id = '$id' ");
		$this->db->query("INSERT INTO friends(user_1_id,user_2_id) VALUES ('$id', '$user_id')");

	}
	function Set_profile_photo(){
		 session_start();
		 $current_id = $_SESSION['user']['id'];
		 $id = $_POST["id"];
		 $set_photo = $this->db->query(" SELECT address FROM photo WHERE id='$id' ")->fetch_all(true);
		 $address = $set_photo[0]['address'];
		 print $address;
		 $this->db->query(" UPDATE user SET photo='$address' WHERE id='$current_id' ");
		 $_SESSION['user']['photo']=$address;

	}

	function getUser(){
		$id=$_POST["id"];
		$user=$this->db->query(" SELECT * FROM user WHERE id='$id' ")->fetch_all(true);
		print json_encode($user);
	}
}
new Project();


?>