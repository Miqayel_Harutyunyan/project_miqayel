$( document ).ready(function() {
	function getPhotos(){
		$.ajax({
			url:"server.php",
			type:"post",
			data:{ajax:"getPhotos"},
			success:function(r){
				r=JSON.parse(r);
				console.log(r[0]["address"]);
				for(i in r){
					
					$(`	
						<div class="image1">
						<a href="${r[i]['address']}" data-lightbox="roadtrip">
						<img src="${r[i]['address']}" class="img-thumbnail" />
						</a>
						<div>
						<button class="set_profile_photo btn btn-success" id="${r[i].id}">Profile</button>
						<button class="delete_this_photo btn btn-success" id="${r[i].id}">Delete</button>
						</div>
						</div>
						`).appendTo(".photos");
				}
			}
		})
	};
	getPhotos();
	$(document).on("click",".set_profile_photo",function() {
		let id = $(this).attr('id');
		$.ajax({
			url:"server.php",
			type:"post",
			data:{ajax:"set_profile_photo",id:id},
			success:function(r){
				// r=JSON.parse(r);
				console.log(r);
			$("#change_profile_photo").attr('src',r);

			},
		})
	}); 
	// 	$(document).on("click",".delete_this_photo",function() {
	// 	let id = $(this).attr('id');
	// 	$.ajax({
	// 		url:"server.php",
	// 		type:"post",
	// 		data:{ajax:"delete_this_photo",id:id},
	// 		success:function(r){
	// 			// r=JSON.parse(r);
	// 			console.log(r);
	// 		// $("#change_profile_photo").attr('src',r);

	// 		},
	// 	})
	// });    
});
