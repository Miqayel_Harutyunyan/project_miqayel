$(document).ready(function () {
	$("#register").click(function(){
		let user={
			name:$("#name").val(),
			surname:$("#surname").val(),
			age:$("#age").val(),
			email:$("#email").val(),
			password:$("#password").val(),
			cpassword:$("#cpassword").val(),
		}
		$.ajax({
			url:"server.php",
			type:"post",
			data:{ajax:"register",user:user},
			success:function(r){
				$(".errors").remove();
				if (r != "success") {
					r = JSON.parse(r);
					console.log(r);
					for(el in r){
						console.log(el);
						$("#"+el).before(`<label class="alert alert-danger errors">${r[el]}</label>`);
					}
				}else{
					Swal.fire(
						'Good job!',
						'Welcome to Our website!',
						'success'
						)
					setTimeout(function(){
						location.href = "index.php"
					},1000)
				}
			}
		})
	})
})