$(document).ready(function () {
	$("#login").click(function(){
		let user={
			email:$("#email").val(),
			password:$("#password").val()
		}
		$.ajax({
			url:"server.php",
			type:"post",
			data:{ajax:"login",user:user},
			success:function(r){
				console.log(r)
				$(".errors").remove();
				if (r != "success") {
					r = JSON.parse(r);
					console.log(r);
					for(el in r){
						console.log(el);
						$("#"+el).before(`<label class="alert alert-danger errors">${r[el]}</label>`);
					}
				}else{
					Swal.fire(
						'Yo have logged in!',
						'You are logged in',
						'success'
						)
					setTimeout(function(){
						location.href = "profile.php"
					},1000)
				}
			}
		})
	})
})