$(document).ready(function () {
	$("#password_change").click(function() {
		let user={
			old_password:$("#old_password").val(),
			new_password:$("#new_password").val(),
			confirm_new_password:$("#confirm_new_password").val(),
		}
		$.ajax({
			url:"server.php",
			type:"post",
			data:{ajax:"password_change",user:user},
			success:function(r){
				console.log(r)
				$(".errors").remove();
				if (r != "success") {
					r = JSON.parse(r);
					for(el in r){
						$("#"+el).after(`<label class="alert alert-danger errors">${r[el]}</label>`);
					}
				}else{
					// Swal.fire(
					// 	'Good job!',
					// 	'Your password changed',
					// 	'success',
					// 	)
					setTimeout(function(){
						location.href = "profile.php"
					},1000)
				}
			}
		})
	});
})