$("#search").on("input",function(e){
	e.preventDefault()
// .trim() prabelneri hamar
let stxt = $(this).val().trim()
if (stxt.length > 0) {
	$("#sbox").show(200)
	$.ajax({
		url:"server.php",
		type:"post",
		data:{ajax:"search",search:stxt},
		success:function(r){
			$(".sresult").empty()
			r = JSON.parse(r)
			console.log(r)
			if (r.length > 0) {
				for(e in r){
					if(r[e]["info"] == "friends"){
						$(".sresult").append(`
							<li class="list-group-item border">
							${r[e].name} ${r[e].surname}
							<div class="sres">
							<button class="btnAdd1" value="${r[e].id}">
							<i class='fas fa-user-alt' style='font-size:24px'></i>
							</button>
							</div>
							</li>
							`)
					}
					else if(r[e]["info"] == "req_to"){
						$(".sresult").append(`
							<li class="list-group-item border">
							${r[e].name} ${r[e].surname}
							<div class="sres">
							<button class="btnDel" value="${r[e].id}">x</button>
							</div>
							</li>
							`)
					}
					else if(r[e]["info"] == "req_me"){
						$(".sresult").append(`
							<li class="list-group-item border">
							${r[e].name} ${r[e].surname}
							<div class="sres">
							<button class="btnSelect" value="${r[e].id}">v</button>
							<button value="${r[e].id}" class="btnRefuse">x</button>
							</div>
							</li>
							`)
					}
					else {
						$(".sresult").append(`
							<li class="list-group-item border">
							${r[e].name} ${r[e].surname}
							<div class="sres">
							<button class="btnAdd" value="${r[e].id}">+</button>
							</div>
							</li>
							`)
					}
				}

			}
			else {
				$(".sresult").append(`
					<li class="list-group-item border">no result</li>
					`)
			}
		}
	})
}
else {
	$("#sbox").hide(200)
}
})