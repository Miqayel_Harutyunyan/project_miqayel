$(document).ready(function () {
	$("#edit_profile").click(function() {
		let user={
			name:$("#name").val(),
			surname:$("#surname").val(),
			email:$("#email").val(),
			age:$("#age").val(),
		}
		$.ajax({
			url:"server.php",
			type:"post",
			data:{ajax:"edit_profile",user:user},
			success:function(r){
				$(".errors").remove();
				if (r != "success") {
					r = JSON.parse(r);
					console.log(r);
					for(el in r){
						console.log(el);
						$("#"+el).after(`<label class="alert alert-danger errors">${r[el]}</label>`);
					}
				}else{
					// Swal.fire(
					// 	'Good job!',
					// 	'Your info changed',
					// 	'success',
					// 	)
					setTimeout(function(){
						location.href = "profile.php"
					},1000)
				}
			}
		})
	});
})