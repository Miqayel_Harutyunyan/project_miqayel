$(document).ready(function () {
	$("#search").on('input', function () {
		let stxt = $(this).val().trim();
		//console.log(stxt);
		if (stxt.length>0) {
			$("#sbox").show(200);
			$.ajax({
				url:"server.php",
				type:"post",
				data:{ajax:"search",search:stxt},
				success:function(r){
					$(".sresult").empty()
					r = JSON.parse(r)
					console.log(r)
					if (r.length > 0) {
						for(e in r){
							if(r[e]["info"] == "friends"){
								$(".sresult").append(`
									<li class="list-group-item text-dark border">
									<a href= "user.php?id=${r[e].id}">${r[e].name} ${r[e].surname}</a>
										<div class="sres">
											<button class="btnAdd1" id="${r[e].id}">
												<i class='fas fa-user-alt' style='font-size:24px'></i>
											</button>
										</div>
									</li>
									`)
							}
							else if(r[e]["info"] == "req_to"){
								$(".sresult").append(`
									<li class="list-group-item text-dark border">
									<a href= "user.php?id=${r[e].id}">${r[e].name} ${r[e].surname}</a>
									<div class="sres">
									<button class="btnDel" id="${r[e].id}">x</button>
									</div>
									</li>
									`)
							}
							else if(r[e]["info"] == "req_me"){
								$(".sresult").append(`
									<li class="list-group-item text-dark border">
									<a href= "user.php?id=${r[e].id}">${r[e].name} ${r[e].surname}</a>
									<div class="sres">
									<button class="btnSelect" id="${r[e].id}">v</button>
									<button id="${r[e].id}" class="btnRefuse">x</button>
									</div>
									</li>
									`)
							}
							else {
								$(".sresult").append(`
									<li class="list-group-item text-dark border">
									<a href= "user.php?id=${r[e].id}">${r[e].name} ${r[e].surname}</a>
									<div class="sres">
									<button class="btnAdd" id="${r[e].id}">+</button>
									</div>
									</li>
									`)
							}
						}

					}
					else {
						$(".sresult").append(`
							<li class="list-group-item border">no result</li>
							`)
					}
				},
			});
		}
	});
	$(document).on("click", ".btnAdd", function() {

		let id = $(this).attr('id')
		let parent = $(this).parents('.sres')
		parent.empty()
		parent.append(`<button class="btnDel" id="${ id }">x</button>`)
		$.ajax({
			url:"server.php",
			type: 'post',
			data :{ajax:"sendRequest", id:id},
			success:function(r){
				console.log(r)
			}
		})

	});


	$(document).on("click", ".btnDel", function() {

		let id = $(this).attr('id')
		let parent = $(this).parents('.sres')
		parent.empty()
		parent.append(`<button class="btnAdd" id="${ id }">+</button>`)
		$.ajax({
			url:"server.php",
			type: 'post',
			data :{ajax:"deleteRequest", id:id},
			success:function(r){
				console.log(r)
			}
		})

	});
	$(document).on("click", ".btnSelect", function() {

		let id = $(this).attr('id')
		let parent = $(this).parents('.sres')
		parent.empty()
		$.ajax({
			url:"server.php",
			type: 'post',
			data :{ajax:"acceptRequest", id:id},
			success:function(r){
				console.log(r)
				parent.append(`<button class="btnAdd1">
					<i class='fas fa-user-alt' style='font-size:24px'></i>
				</button>`)
			}
		})

	});



});