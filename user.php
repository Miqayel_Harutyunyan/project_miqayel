<?php require_once 'header.php';?>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<input type="hidden" value="<?= $_GET['id'] ?>" id="user_id">
<div class="container emp-profile">
	<div>
		<div class="row">
			<div class="col-md-4">
				<div class="profile-img">
					<img id="change_profile_photo" src="https://www.knack.com/images/about/default-profile.png" alt="profile image"/>
					
				</div>
			</div>
			<div class="col-md-6">
				<div class="profile-head">
					<h5>
						<?= $user["name"] ?>
					</h5>
					<h6>
						<?= $user["surname"] ?>
					</h6>
					<p class="proile-rating">Age : <span><?= $user["age"] ?></span></p>
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Timeline</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profile-tab" data-toggle="tab" href="#photo" role="tab" aria-controls="profile" aria-selected="false">Photos</a>
						</li>
					</ul>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="profile-work">
					<p>WORK LINK</p>
					<a href="">Website Link</a><br/>
					<a href="">Bootsnipp Profile</a><br/>
					<a href="">Bootply Profile</a>
					<p>SKILLS</p>
					<a href="">Web Designer</a><br/>
					<a href="">Web Developer</a><br/>
					<a href="">WordPress</a><br/>
					<a href="">WooCommerce</a><br/>
					<a href="">PHP, .Net</a><br/>
				</div>
			</div>
			<div class="col-md-8">
				<div class="tab-content profile-tab" id="myTabContent">
					<div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
						
						<div class="row">
							<div class="col-md-6">
								<label>Name</label>
							</div>
							<div class="col-md-6">
								<p><?= $user["name"] ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Email</label>
							</div>
							<div class="col-md-6">
								<p><?= $user["email"] ?></p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Age</label>
							</div>
							<div class="col-md-6">
								<p><?= $user["age"] ?></p>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
						<div class="row">
							<div class="col-md-6">
								<label>Experience</label>
							</div>
							<div class="col-md-6">
								<p>Expert</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Hourly Rate</label>
							</div>
							<div class="col-md-6">
								<p>10$/hr</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Total Projects</label>
							</div>
							<div class="col-md-6">
								<p>230</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>English Level</label>
							</div>
							<div class="col-md-6">
								<p>Expert</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<label>Availability</label>
							</div>
							<div class="col-md-6">
								<p>6 months</p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<label>Your Bio</label><br/>
								<p>Your detail description</p>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="photo" role="tabpanel" aria-labelledby="profile-tab">
						<form action="server.php" method="post" enctype="multipart/form-data">
							<input name="photo" type="file"  accept="image/*"> 
							<div class="wrapper">
								<!-- <label for="file" class="btn-3">
									<span>Add photo</span>
								</label> -->
								<button class="btn btn-success">Add</button>
							</div>
						</form>	
						<div class="photos">
						<!-- <a href="images/image-2.jpg" data-lightbox="roadtrip">Image #2</a>
						<a href="images/image-3.jpg" data-lightbox="roadtrip">Image #3</a>
						<a href="images/image-4.jpg" data-lightbox="roadtrip">Image #4</a> -->
						
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>           
</div>
<script src="lightbox/dist/js/lightbox.min.js"></script>

<script src="js/user.js"></script>
<?php require_once 'footer.php';?>