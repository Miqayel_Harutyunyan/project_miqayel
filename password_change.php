<?php require_once 'header.php';?>
<div class="container">

	<div class="well form-horizontal"   id="contact_form">
		<fieldset>

			<!-- Form Name -->
			<legend><center><h2><b>Change password</b></h2></center></legend><br>

			<!-- Text input-->

			<div class="form-group">
				<label class="col-md-4 control-label">Old password</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input id="old_password" name="first_name" placeholder="Old password" class="form-control"  type="text">
					</div>
				</div>
			</div>

			<!-- Text input-->

			<div class="form-group">
				<label class="col-md-4 control-label" >New password</label> 
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
						<input id="new_password" name="last_name" placeholder="New password" class="form-control"  type="text">
					</div>
				</div>
			</div>

			<!-- Text input-->

			<div class="form-group">
				<label class="col-md-4 control-label">Confirm password</label>  
				<div class="col-md-4 inputGroupContainer">
					<div class="input-group">
						<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
						<input id="confirm_new_password" name="email" placeholder="Confirm Password" class="form-control"  type="text">
					</div>
				</div>
			</div>
			<!-- Text input-->
			<!-- Button -->
			<div class="form-group">
				<label class="col-md-4 control-label"></label>
				<div class="col-md-4"><br>
					<button id="password_change" type="submit" class="btn btn-warning" > <span class="glyphicon glyphicon-send">Change password</span></button>
				</div>
			</div>

		</fieldset>
	</div>
</div>
</div><!-- /.container -->
<script src="js/password_change.js"></script>

<?php require_once 'footer.php';?>