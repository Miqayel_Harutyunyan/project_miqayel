<?php require_once 'header.php';?>
<div class="container">

<!-- Form Name -->
<legend><center><h2><b>Edit profile</b></h2></center></legend><br>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label">Name</label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  id="name" placeholder="Name" class="form-control" value="<?= $user["name"] ?>" type="text">
    </div>
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" >Surname</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input id="surname" placeholder="Surname" class="form-control" value="<?= $user["surname"] ?>" type="text">
    </div>
  </div>
</div>
<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input id="email" placeholder="E-Mail Address" class="form-control" value="<?= $user["email"] ?>" type="text">
    </div>
  </div>
</div>
<!-- Text input--> 
<div class="form-group">
  <label class="col-md-4 control-label">Age</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input id="age" placeholder="Age" class="form-control" type="number" value="<?= $user["age"] ?>">
    </div>
  </div>
</div>
<!-- Select Basic -->
<!-- Button -->
<div >
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4"><br>
    <button id="edit_profile" class="btn btn-warning">Edit</button>
  </div>
</div>
</div>
    </div><!-- /.container -->

<script src="js/edit_profile.js"></script>
  

<?php require_once 'footer.php';?>